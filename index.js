const express = require('express')
const jimp = require('jimp');

const config = {
  base: './assets/images/base.png',
  font: './assets/font/font.fnt',
  right: 512,
  top: 207,
};

const app = express();
const port = process.env.PORT || 3000;

const generateImage = async (text) => {
  const image = await jimp.read(config.base);
  const font = await jimp.loadFont(config.font);

  const width = await jimp.measureText(font, text);
  const height = await jimp.measureTextHeight(font, text);

  image.print(
    font,
    512 - width / 2,
    207 - height / 2,
    text,
  );

  return image;
};

app.get('/textoo', async (req, res) => {
  let { text } = req.query;

  if (!text) {
    text = 'Tap to Type';
  }

  if (text.length > 32) {
    text = text.substr(0, 32);
  }

  const image = await generateImage(text);
  const buffer = await image.getBufferAsync(jimp.MIME_PNG);

  res.setHeader('Content-Type', jimp.MIME_PNG);
  res.send(buffer);
});

app.listen(port, () => {
  console.log(`Listening at :${port}`);
});
